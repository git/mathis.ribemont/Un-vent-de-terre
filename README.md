# Un Vent De Terre 🐛

## Getting Started 🔥🚀

To start the project: 
* Open [UnVentDeTerre.sln](Sources/UnVentDeTerre.sln)
* Right click on the solution
* Define Starting projects...
* Check Multiple Starting Projects in "Starting Project"
* Set "Minecraft.Crafting.Api" and "UnVentDeTerre" to **Start**
* The project now running, you can use the app

## Features 🎉

* ✅ Crafting Table
* ✅ Inventory Page
    * ✅ Accesible from the "inventory" URL
    * ✅ Display image or Alt text for each item
    * ✅ Record all user actions
    * ✅ Text of the page in English/French
    * ✅ Item List
        * ✅ Pagination
        * ✅ Name and Image displayed
        * ✅ Search field
        * ✅ Items Sortable by their names
    * ✅ Inventory
        * ✅ Persistent
        * ✅ Item position persistent too
        * ✅ Move item outside = delete
        * ✅ Add Item by dragging dropping from a list of aviable items
        * 💥 Item not replacable
        * 💥 StackSize data
        * 💥 Cut half stack
        * 💥 Inventory Size in config file
* 💥 Localization

# How the client solution works ?

The client solution works with Blazor Server, this is a technology that enable us to use C# to create Web Apps. It is divided into multiple parts. </br>
* Firstly, we can found Pages folder, inside, there is all the pages of the App. </br>
* Secondly, we can found the Component folder that contains all app components used in pages (In this folder, we can found the Inventory and Crafting complex components for example). We tried to divide the application in a lot of component to make it easier to maintain. For exemple, we try to create a configurable list of item to use it in several page, but with different feature. So, we can choose to make item in list non-draggable with a boolean parameter in the list.</br>
* Next, we can found the data access layer in the folder Services, it talks with a Restful API that persist the inventory contents, give the list of avalaible items, ... </br>
* Then, we can found the CultureController in the Controllers folder, this one is used to translate text into multiple langages by using ressources files that are in Ressources folder. </br>

## Authors 👷
* DUBOIS Mickaël PMB2
* Mathis RIBEMONT PMA1
