﻿using System;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using UnVentDeTerre.Models;
using UnVentDeTerre.Services;

namespace UnVentDeTerre.Component
{
	public partial class Inventory
	{
        [Inject]
        public IDataService DataService { get; set; }

        public ObservableCollection<ItemAction> Actions { get; set; }
        public Item CurrentDragItem { get; set; }

        [Parameter]
        public List<Item> Items { get; set; }

        private int totalItems;

        public List<Item> InventoryItems { get; set; }

        /// <summary>
        /// Gets or sets the java script runtime.
        /// </summary>
        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        protected async override Task OnInitializedAsync()
        {
            var inventoryModels = await DataService.GetInventoryContent();
            totalItems = await DataService.Count();
            foreach (var invM in inventoryModels)
            {
                this.InventoryItems[invM.Position] = new Item() { Name = invM.ItemName, DisplayName = invM.ItemName, StackSize = invM.NumberItem, ImageBase64 = invM.ImageBase64 };
            }
        }

        public Inventory()
        {
            Actions = new ObservableCollection<ItemAction>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
            this.InventoryItems = new List<Item> { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null };
        }
        
        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            JavaScriptRuntime.InvokeVoidAsync("Crafting.AddActions", e.NewItems);
        }
    }
}

