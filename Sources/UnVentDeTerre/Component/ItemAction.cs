﻿using System;
using UnVentDeTerre.Models;

namespace UnVentDeTerre.Component
{
    public class ItemAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}

