﻿using System;
using UnVentDeTerre.Models;

namespace UnVentDeTerre.Component
{
    public class CraftingRecipe
    {
        public Item Give { get; set; }
        public List<List<string>> Have { get; set; }
    }
}

