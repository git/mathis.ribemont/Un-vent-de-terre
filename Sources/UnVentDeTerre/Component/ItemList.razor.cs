using System.Collections.ObjectModel;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using UnVentDeTerre.Models;
using UnVentDeTerre.Pages;
using UnVentDeTerre.Services;

namespace UnVentDeTerre.Component;

public partial class ItemList
{
    private DataGrid<Item> dataGrid;
    
    [Parameter] 
    public List<Item> Items { get; set; } = new();
    
    [Parameter]
    public EventCallback<DataGridReadDataEventArgs<Item>> OnReadData { get; set; }

    [Parameter]
    public int TotalItems { get; set; }

    [Parameter] public Action<int>? OnDelete { get; set; } = null;
    
    private string customFilterValue;

    [Parameter] 
    public bool itemEditable { get; set; }
    
    [Parameter]
    public bool itemDraggable { get; set; }

    private Task OnCustomFilterValueChanged( string e )
    {
        customFilterValue = e;
        return dataGrid.Reload();
    }

    private bool OnCustomFilter( Item model )
    {
        // We want to accept empty value as valid or otherwise
        // datagrid will not show anything.
        if ( string.IsNullOrEmpty( customFilterValue ) )
            return true;

        return model.Name.Contains( customFilterValue, StringComparison.OrdinalIgnoreCase );
    }
}