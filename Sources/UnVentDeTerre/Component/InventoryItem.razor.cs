﻿using System;
using Microsoft.AspNetCore.Components;
using UnVentDeTerre.Models;

namespace UnVentDeTerre.Component
{
	public partial class InventoryItem
	{
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public Inventory Parent { get; set; }

        private bool isOutSide = false;

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }
            isOutSide = false;
            Parent.Actions.Add(new ItemAction { Action = "Drag Enter", Item = Item, Index = Index });
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }
            isOutSide = true;
            Parent.Actions.Add(new ItemAction { Action = "Drag Leave", Item = Item, Index = Index });
        }

        internal void OnDragEnd()
        {
            Parent.Actions.Add(new ItemAction { Action = "Drag End", Item = Item, Index = Index });
            if (isOutSide)
            {
                Parent.Actions.Add(new ItemAction { Action = "Delete Item", Item = Item, Index = Index });
                Parent.InventoryItems[Index] = null;
                Parent.DataService.RemoveInInventory(new InventoryModel() { ItemName = Item.Name, Position = Index, NumberItem = Item.StackSize, ImageBase64 = this.Item.ImageBase64 });
                Item = null;
            }
        }

        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }
            Item = Parent.CurrentDragItem;
            Parent.InventoryItems[this.Index] = this.Item;
            Parent.DataService.AddInInventory(new InventoryModel() { ItemName = this.Item.Name, Position = this.Index, NumberItem = this.Item.StackSize, ImageBase64 = this.Item.ImageBase64 });
            Parent.Actions.Add(new ItemAction { Action = "Drop", Item = this.Item, Index = this.Index });
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = Item;

            Parent.Actions.Add(new ItemAction { Action = "Drag Start", Item = this.Item, Index = this.Index });
        }
    }
}

