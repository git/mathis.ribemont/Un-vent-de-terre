﻿using System;
using Blazorise;
using UnVentDeTerre.Models;
using Microsoft.AspNetCore.Components;

namespace UnVentDeTerre.Component
{
    public partial class CraftingItem
    {

        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public Crafting Parent { get; set; }

        [Inject]
        public ILogger<CraftingItem> Logger { get; set; }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new ItemAction { Action = "Drag Enter", Item = this.Item, Index = this.Index });
            Logger.LogInformation($"CraftingItem: OnDragEnter");
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new ItemAction { Action = "Drag Leave", Item = this.Item, Index = this.Index });
            Logger.LogInformation($"CraftingItem {Item.Name}: OnDragLeave");
        }

        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }

            this.Item = Parent.CurrentDragItem;
            Parent.RecipeItems[this.Index] = this.Item;

            Parent.Actions.Add(new ItemAction { Action = "Drop", Item = this.Item, Index = this.Index });

            // Check recipe
            Parent.CheckRecipe();
            Logger.LogInformation($"CraftingItem {Item.Name}: OnDrop");
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = this.Item;

            Parent.Actions.Add(new ItemAction { Action = "Drag Start", Item = this.Item, Index = this.Index });
            Logger.LogInformation($"CraftingItem {Item.Name}: OnDragStart");
        }
    }
}

