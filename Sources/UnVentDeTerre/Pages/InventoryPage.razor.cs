﻿using System;
using Microsoft.AspNetCore.Components;
using UnVentDeTerre.Models;
using UnVentDeTerre.Services;

namespace UnVentDeTerre.Pages
{
	public partial class InventoryPage
	{
        [Inject]
        public IDataService DataService { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
            StateHasChanged();

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());

            StateHasChanged();
        }
    }
}

