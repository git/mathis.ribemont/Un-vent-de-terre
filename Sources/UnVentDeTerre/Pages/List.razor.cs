﻿using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using UnVentDeTerre.Models;
using UnVentDeTerre.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.VisualBasic.CompilerServices;

namespace UnVentDeTerre
    .Pages
{
    public partial class List
    {
        private List<Item> _items = new();

        private int totalItem;

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }

        [CascadingParameter]
        public IModalService Modal { get; set; }

        protected override async Task<Task> OnInitializedAsync()
        {
            _items = await DataService.List(1, int.MaxValue);
            totalItem = await DataService.Count();
            return base.OnInitializedAsync();
        }

        private async void OnDelete(int id)
        {
            var parameters = new ModalParameters();
            parameters.Add(nameof(Item.Id), id);

            var modal = Modal.Show<DeleteConfirmation>("Delete Confirmation", parameters);
            var result = await modal.Result;

            if (result.Cancelled)
            {
                return;
            }

            await DataService.Delete(id);

            // Reload the page
            NavigationManager.NavigateTo("list", true);
        }
    }
}
