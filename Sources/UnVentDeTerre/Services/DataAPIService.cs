using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using UnVentDeTerre.Component;
using UnVentDeTerre.Factory;
using UnVentDeTerre.Models;

namespace UnVentDeTerre.Services;

public class DataAPIService : IDataService
{
    private readonly HttpClient _http;

    public DataAPIService(
        HttpClient http)
    {
        _http = http;
    }

    public async Task Add(ItemModel model)
    {
        // Get the item
        var item = ItemFactory.Create(model);
        
        // Save the data
        await _http.PostAsJsonAsync("https://localhost:7234/api/Crafting/", item);
    }

    public async Task<int> Count()
    {
        return await _http.GetFromJsonAsync<int>("https://localhost:7234/api/Crafting/count");
    }

    public async Task<List<Item>> List(int currentPage, int pageSize)
    {
        return await _http.GetFromJsonAsync<List<Item>>($"https://localhost:7234/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
    }
    
    public async Task<Item> GetById(int id)
    {
        return await _http.GetFromJsonAsync<Item>($"https://localhost:7234/api/Crafting/{id}");
    }

    public async Task Update(int id, ItemModel model)
    {
        // Get the item
        var item = ItemFactory.Create(model);
        
        await _http.PutAsJsonAsync($"https://localhost:7234/api/Crafting/{id}", item);
    }

    public async Task Delete(int id)
    {
        await _http.DeleteAsync($"https://localhost:7234/api/Crafting/{id}");
    }

    public async Task<List<CraftingRecipe>> GetRecipes()
    {
        return await _http.GetFromJsonAsync<List<CraftingRecipe>>("https://localhost:7234/api/Crafting/recipe");
    }

    public async Task AddInInventory(InventoryModel model)
    {
        // Save the data
        await _http.PostAsJsonAsync("https://localhost:7234/api/Inventory/", model);
    }

    public async Task<List<InventoryModel>> GetInventoryContent()
    {
        return await _http.GetFromJsonAsync<List<InventoryModel>>("https://localhost:7234/api/Inventory/");
    }

    public async Task RemoveInInventory(InventoryModel model)
    {
        var stringContent = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
        var request = new HttpRequestMessage(HttpMethod.Delete, "https://localhost:7234/api/Inventory/");
        request.Content = stringContent;
        await _http.SendAsync(request);
    }
}